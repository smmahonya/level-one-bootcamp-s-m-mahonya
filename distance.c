//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include <math.h>
int inputs()
{
    double s;
    scanf("%lf",&s);
    return s;
}

void print (int m1, int n1, int m2, int n2,double dist)
{
    printf("Distance between (%d , %d) and  (%d , %d)   = %f\n",m1,n1,m2,n2,dist);
}

double calc(double m1, double n1,double m2,double n2)
{
     return (sqrt(( (m2-m1)*(m2-m1) )+( (n2-n1)*(n2-n1) )));
}
int main() 
{
    double m1, n1, m2, n2;
    
    printf("Enter the value of m1: ");
    m1=inputs();
    
    printf("Enter the value of n1: ");
    n1=inputs();
    
    printf("Enter the value of m2: ");
    m2=inputs();
    
    printf("Enter the value of n2: ");
    n2=inputs();
    double dist = calc(m1,n1,m2,n2);
    print(m1,n1,m2,n2,dist);
    return 0;
}
