//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct coordinate
{
    double x,y;
};
typedef struct coordinate point;
point input();
double distance(point m1,point m2);
void output(double Dist);

int main()
{
    point m1,m2;
    double Dist;
    printf("Enter the coordinate of 1st point\n");
    m1=input();
    printf("Enter the coordinate of 2nd point\n");
    m2=input();
    Dist=distance(m1,m2);
    output(Dist);
    return 0;
}
point input()
{
    point z;
    printf("Enter x coordinate: ");
    scanf("%lf",&z.x);
    printf("Enter y coordinate: ");
    scanf("%lf",&z.y);
    return z;
}
double distance(point m1,point m2)
{
    return(sqrt(((m2.x-m1.x)*(m2.x-m1.x))+((m2.y-m1.y)*(m2.y-m1.y))));
}
void output(double Dist)
{
    printf("The distance between two points is:\n%lf",Dist);
}