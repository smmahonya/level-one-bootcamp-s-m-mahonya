//WAP to find the sum of n fractions.
#include <stdio.h>
struct fract
{
  int M;
  int Z;
};
typedef struct fract Fractions;

Fractions
getdata (int A)
{
  Fractions FractionsInput;
  printf ("Enter the number %d numerator value  : ", A);
  scanf ("%d", &FractionsInput.M);
  printf ("Enter the number %d denominator value: ", A);
  scanf ("%d", &FractionsInput.Z);
  return FractionsInput;
}

void
getN (int M, Fractions s[M])
{
  for (int A = 0; A < M; A++)
    {
      s[A] = getdata (A+1);
    }
}

int
gcd (int M, int Z)
{
  if (Z != 0)
    return gcd (Z, M % Z);
  else
    return M;
}

Fractions
computeOne (Fractions s1, Fractions s2)
{
  Fractions Computevalue;
  int gcdvalue;
  Computevalue.M = (s1.M * s2.Z) + (s1.Z * s2.M);
  Computevalue.Z = (s1.Z * s2.Z);
  gcdvalue = gcd (Computevalue.M, Computevalue.Z);
  Computevalue.M = (Computevalue.M / gcdvalue);
  Computevalue.Z = (Computevalue.Z / gcdvalue);
  return Computevalue;
}

Fractions
computeN (int M, Fractions s[M])
{
  Fractions result;
  result.M = 0;
  result.Z = 1;
  for (int A = 0; A < M ; A++)
    {
      result = computeOne (result, s[A]);
    }
  return result;
}

int
GetnumberOfFractions ()
{
  int M;
  printf ("Enter the number of fractions: ");
  scanf ("%d", &M);
  return M;
}

void
displayOutput (Fractions sum, int M)
{
  printf ("The sum of %d Fractions is  %d/%d ", M, sum.M, sum.Z);
}

int
main ()
{
  Fractions SumOfFractions;
  int M;
  M = GetnumberOfFractions ();
  Fractions s[M];
  getN (M, s);
  SumOfFractions = computeN (M, s);
  displayOutput (SumOfFractions, M);
}

