//WAP to find the sum of two fractions.
#include <stdio.h>
struct fract
{
    int M, N;
};
typedef struct fract Fraction;
int gcd(int M, int N)
{
    if (M == 0)
        return N;
    return gcd(N % M, M);
}

int lcm(int M,int N)
{
    return ((M*N)/gcd(M,N)); 
}
Fraction input()
{
    Fraction Q;
    printf("Enter the value of Numerator:");
    scanf("%d",&Q.M);
    printf("Enter the value of Denominator:");
    scanf("%d",&Q.N);
    return Q;
}
Fraction compute(Fraction RE,Fraction Q)
{
    Fraction sum;
    int l = lcm(RE.N,Q.N);
    sum.M = RE.M*(l/RE.N)  + Q.M*(l/Q.N);
    sum.N = l;
    return sum;
};
void output(Fraction RE)
{
    int g=gcd(RE.M,RE.N);
    RE.M/=g;
    RE.N/=g;
    printf("The sum of two Fractions is: %d/%d",RE.M,RE.N);
}
int main(void)
{   
    Fraction Q1,Q2,RE;
    
    printf("Enter 1st fraction:\n");
    Q1 = input();
    printf("Enter 2nd fraction:\n");
    Q2= input();
    RE=compute(Q1,Q2);
    output(RE);
    return 0;
}
