//Write a program to find the sum of n different numbers using 4 functions
#include <stdio.h>
float input_ab() 
{
    float M;
    printf ("Enter a number: ");
    scanf ("%f", &M);
    if(M<0)
    {
        printf("Enter a positive number:");
        return input_ab();
    }
    return M;
}

void input_array(int ab, int M[ab]) 
{
    for (int i = 0; i < ab; i++)
    {
    printf ("Enter the element number %d of the array: ", i+1);
    scanf ("%d", &M[i]);
    } 
} 
 
int

find_array_sum (int ab, int M[ab]) 
{
    int sum = 0;
    for (int i = 0; i < ab; i++)
    {
        sum += M[i];
    } 
    return sum;
}
void show_results(int ab, int M[ab], int sum) 
{
    int i;
    printf ("The sum of\n");
    for (i = 0; i < ab-1; i++)
    {
        printf ("%d+", M[i]);
    }
    printf ("%d=%d", M[i], sum);
}

int main () 
{
    int ab, sum;
    ab = input_ab ();
    int M[ab];
    input_array (ab, M);
    sum = find_array_sum (ab, M);
    show_results (ab, M, sum);
    return 0;
}


